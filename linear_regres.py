"""

@author: tunde
"""


import numpy as np
import matplotlib.pylab as plt

      
def Vand(xData, ncol):
    xData = np.array(xData)
    m = len(xData)
    V = np.zeros((m, ncol+1))
    V[:,0] =  np.ones(m)
    for j in range(1, ncol+1):
        V[:,j] = V[:, j-1]* xData

    print('V.shape={},  condition number of V={}'.format(V.shape, np.linalg.cond(V)))
    return V
    

def poly_eval(c, x):
    p = c[0]
    for i in range(1, len(c)):
        p += c[i]*x**i
    return p
        
        
def lin_lstsq(xData, yData, x, m):
    V = Vand(xData, m)
    x = np.array(x)
    w = np.linalg.lstsq(V, yData)[0]
    print('m=', m,  ',  w=', w)
    return  poly_eval(w, x)
      

#
# general least square (using two general basis {f2, f2} as an example)
#
def G_lin_lstsq(xData, yData, x, f1, f2):
    V = G_Vand(xData, f1, f2)
    x = np.array(x)
    w = np.linalg.lstsq(V, yData)[0]
    print('m=', V.shape[-1],  'w=', w)
    #return  poly_eval(w, x)
    return  w[0]*f1(x) + w[1]*f2(x)


def G_Vand(xData, f1, f2):
    xData = np.array(xData)
    m = len(xData)
    V = np.zeros((m, 2))
    V[:,0] = f1(xData)
    V[:,1] = f2(xData)
    print(V.shape)
    return V





#=================================================================
if __name__=='__main__':

    n = 30
    x = np.linspace(-4, 4, n )

    #f = lambda x:  x**2   #2 * x - 5  + x**3 - x**5/1000
    f1 = lambda x : np.exp(x*0.7)
    f2 = lambda x : np.exp(-x*0.5)
    f = lambda x : f1(x) + f2(x)

    fx = f(x) + np.random.randn(len(x))*2

    xL = np.linspace(-6, 5, 200)    
    lreg = lin_lstsq(x, fx, xL, 1)
    sqreg = lin_lstsq(x, fx, xL, 2)
    reg5 = lin_lstsq(x, fx, xL, 5)
    pinter = lin_lstsq(x, fx, xL, n-1)     #interpolation 
    Greg = G_lin_lstsq(x, fx, xL, f1, f2)

    plt.plot(x, fx, 'rp', ms=6, label='data points')
    plt.plot(xL, f(xL), 'b-.', lw=3, label='curve')
    plt.plot(xL, lreg, 'g-', lw=2, label='lin regres. 1st ord')
    plt.plot(xL, sqreg, 'c-', lw=2, label='lin regres., 2nd ord')
    plt.plot(xL, reg5, 'y-', lw=2, label='lin regres., 5th ord')
    #plt.plot(xL, pinter, 'k-', lw=2, label='interp')  #interpolation (overfitting) cannot generalize
    plt.plot(xL, Greg, 'k-', lw=2, label='GLS')
    plt.legend(loc='best')
    plt.show()
