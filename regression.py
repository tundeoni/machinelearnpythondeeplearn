import sklearn.datasets as dt
import numpy as np
import pandas as pd
# import sklearn as sk
import matplotlib.pyplot as plt
import seaborn as sns
import statsmodels.api as sm
import pylab

boston = dt.load_boston()
# print(boston.data[3, 10:], "   ", boston.target[3])
df_boston = pd.DataFrame(boston.data, index=np.arange(506), columns=boston.feature_names)   
df_boston["MEDV"] = pd.Series(boston.target, index=np.arange(506))
# print(boston.DESCR)
# print(df_boston.corr())
# print(df_boston.describe())

def r_squared_adj(r_squared, n, k):
    '''
        r_squared: The coefficient of determination 
        n: The total number of records in the sample
        k: The total number of predictors in the sample
    '''
    val = 1 - ((1 - r_squared) * ((n - 1) / (n - k - 1)))
    return val

def summarize(data): 
    col_count = 0
    cols_df = pd.DataFrame({"A": 0}, index=np.array([0]))
    for cols in data.columns:
        col_type = str(data[cols].dtype)
        if col_count == 0:  
            cols_df = pd.DataFrame({"VAR_NAME": cols, "TYPE": data[cols].dtype, "CORR_Y": np.corrcoef(data[cols], data["MEDV"])[0][1], "MEAN": np.mean(data[cols]), "SKEW": data[cols].skew(), "KURT": data[cols].kurt(), "MIN": np.min(data[cols]), "MAX": np.max(data[cols]), "STDEV": np.std(data[cols]), "COUNT":  len(data[cols]), "MISSING": np.sum(pd.isnull(data[cols])), "TMIN": np.mean(data[cols]) - (3 * np.std(data[cols])), "TMAX": np.mean(data[cols]) + (3 * np.std(data[cols]))}, index=np.array([col_count]))
        else:
            cols_df = cols_df.append(pd.DataFrame({"VAR_NAME": cols, "TYPE": data[cols].dtype, "CORR_Y": np.corrcoef(data[cols], data["MEDV"])[0][1], "MEAN": np.mean(data[cols]), "SKEW": data[cols].skew(), "KURT": data[cols].kurt(), "MIN": np.min(data[cols]), "MAX": np.max(data[cols]), "STDEV": np.std(data[cols]), "COUNT":  len(data[cols]), "MISSING": np.sum(pd.isnull(data[cols])), "TMIN": np.mean(data[cols]) - (3 * np.std(data[cols])), "TMAX": np.mean(data[cols]) + (3 * np.std(data[cols]))}, index=np.array([col_count])))
        col_count += 1
    return cols_df
col_summary = summarize(df_boston)       
col_summary = col_summary[["VAR_NAME", "TYPE", "COUNT", "MISSING", "CORR_Y", "MEAN", "STDEV", "MIN", "MAX", "SKEW", "KURT", "TMIN", "TMAX"]]
col_summary = col_summary.rename(columns=str.lower)
col_summary.sort_values(by="corr_y", ascending=False).head(50)


# In[2]:

# Functions for cleansing and transformations
# df_boston_trans = df_boston.copy()
df_boston_trans = (df_boston
                        .rename(columns=str.lower)
                        .rename(columns={"zn":"zn_new"})
                        .assign(log_lstat=lambda x: np.log10(x['lstat']),
                                    cat_chas=lambda x: pd.Categorical(x['chas'].astype(int)))
                        .drop("lstat", axis=1))
    
# print(pd.crosstab(df_boston_trans.cat_chas, columns="count"))
# print(df_boston_trans.info())


# In[2]:

sm.qqplot(df_boston["RM"], line="q")
pylab.show()


# In[91]:

sm.qqplot(np.log10(df_boston["RM"]), line="q")
pylab.show()


# In[5]:

# sns.plt.title("EDA Plots: Distribution & Correlation")
sns.jointplot(x=np.log10(df_boston["LSTAT"]), y=df_boston["MEDV"], kind="reg")
plt.show()
# plt.savefig("eda_regression.png")


# In[4]:

sns.jointplot(x="LSTAT", y="MEDV", data=df_boston, kind="reg")
plt.show()


# In[20]:

df_boston["rcp_CRIM"] = np.reciprocal(df_boston["CRIM"])
sns.jointplot(x="rcp_CRIM", y="MEDV", data=df_boston, kind="reg")
plt.show()


# In[39]:

sns.jointplot(x="INDUS", y="MEDV", data=df_boston, kind="reg")
plt.show()


# In[38]:

df_boston["log_INDUS"] = np.log10(df_boston["INDUS"])
sns.jointplot(x="log_INDUS", y="MEDV", data=df_boston, kind="reg", dropna=True)
plt.show()


# In[37]:

df_boston["rcp_INDUS"] = np.reciprocal(data["INDUS"])
sns.jointplot(x="rcp_INDUS", y="MEDV", data=df_boston, kind="reg", dropna=True)
plt.show()


# In[2]:

df_boston["log_LSTAT"] = np.log10(df_boston["LSTAT"])
sns.jointplot(x="log_LSTAT", y="MEDV", data=df_boston, kind="reg", dropna=True)
plt.show()


# In[6]:

# Scatter plot of variables RM vs. Target
plt.plot(df_boston["RM"], df_boston["MEDV"], marker=".", linestyle="none")
plt.xlabel("Average Number of Rooms per Dwelling")
plt.ylabel("Median House Value")
plt.xlim(0, 10)
plt.ylim(0, 55)
plt.show()


# In[54]:

# Scatter plot of variables LSTAT vs. Target
plt.plot(df_boston["LSTAT"], df_boston["MEDV"], marker=".", linestyle="none")
plt.xlabel("% Lower Status of the Population")
plt.ylabel("Medium House Value")
plt.xlim(0, 45)
plt.ylim(0, 55)
plt.show()


# In[55]:

# Scatter plot of variables LSTAT vs. RM
plt.plot(df_boston["LSTAT"], df_boston["RM"], marker=".", linestyle="none")
plt.xlabel("% Lower Status of the Population")
plt.ylabel("Average Number of Rooms per Dwelling")
plt.xlim(0, 45)
plt.ylim(0, 10)
plt.show()


# In[9]:

df_boston["log_LSTAT"] = np.log10(df_boston["LSTAT"])


# In[15]:

import sklearn.grid_search as gs
help(gs.GridSearchCV)


# In[24]:

import sklearn.linear_model as lm
from sklearn.preprocessing import StandardScaler
import sklearn.grid_search as gs

# split into input (X) and target (y) variables
X = df_boston[["ZN", "INDUS", "CHAS", "NOX", "RM", "AGE", "DIS", "RAD", "TAX", "PTRATIO", "B", "LSTAT", "log_LSTAT"]]
y = df_boston["MEDV"].values

scalerX = StandardScaler().fit(X)
Stan_X = pd.DataFrame(scalerX.transform(X), index=X.index, columns=X.columns)
scalerY = StandardScaler().fit(y)
Stan_y = scalerY.transform(y)
print(X.index[0:10])

cols_b = ['CHAS', 'NOX', 'RM', 'DIS', 'PTRATIO', 'log_LSTAT']

parameters = {"alpha": [0.0001, 0.001, 0.01, 0.1, 1], "max_iter": [100, 1000, 10000, 100000], "selection": ("random", "cyclic")}
model_lasso = lm.Lasso()
search_model = gs.GridSearchCV(model_lasso, parameters, cv=2)
search_model.fit(Stan_X, Stan_y)

print("\nPrinting LASSO model's results....")

print("\nThe best score from the search: ", search_model.best_score_)
print("The best parameters from the search: ", search_model.best_params_)
# print("R^2 Model B: ", model_lasso_b.score(X[cols_b], y))
# print("R^2adj Model B: ", r_squared_adj(model_lasso_b.score(X[cols_b], y), len(y), len(cols_b)))


# In[25]:

import sklearn.linear_model as lm
from sklearn.preprocessing import StandardScaler

# split into input (X) and target (y) variables
X = df_boston[["ZN", "INDUS", "CHAS", "NOX", "RM", "AGE", "DIS", "RAD", "TAX", "PTRATIO", "B", "LSTAT", "log_LSTAT"]]
y = df_boston["MEDV"].values

scalerX = StandardScaler().fit(X)
Stan_X = pd.DataFrame(scalerX.transform(X), index=X.index, columns=X.columns)
scalerY = StandardScaler().fit(y)
Stan_y = scalerY.transform(y)
print(X.index[0:10])

cols_b = ['CHAS', 'NOX', 'RM', 'DIS', 'PTRATIO', 'log_LSTAT']

model_lasso_b = lm.Lasso(alpha=0.001, max_iter = 100000, selection = "random")
model_lasso_b.fit(Stan_X[cols_b], Stan_y)

print("\nPrinting LASSO model's results....")

print("\nRegression coefficients of Model B: ", model_lasso_b.coef_)
print("Intercept coefficient of Model B: ", model_lasso_b.intercept_)
print("R^2 Model B: ", model_lasso_b.score(Stan_X[cols_b], Stan_y))
print("R^2adj Model B: ", r_squared_adj(model_lasso_b.score(Stan_X[cols_b], Stan_y), len(y), len(cols_b)))


# In[3]:

from sklearn.model_selection import cross_val_score, KFold
from sklearn.linear_model import SGDRegressor
from sklearn.feature_selection import RFECV
from sklearn.preprocessing import StandardScaler

# split into input (X) and target (y) variables
X = df_boston[["ZN", "INDUS", "CHAS", "NOX", "RM", "AGE", "DIS", "RAD", "TAX", "PTRATIO", "B", "LSTAT", "log_LSTAT"]]
y = df_boston["MEDV"].values

scalerX = StandardScaler().fit(X)
Stan_X = pd.DataFrame(scalerX.transform(X), index=X.index, columns=X.columns)
scalerY = StandardScaler().fit(y)
Stan_y = scalerY.transform(y)
print(X.index[0:10])

# Create regression estimator and set hyper-parameters
sgd_reg = SGDRegressor(penalty = "l1", n_iter = 100)

# Create the rfe object to recursively select best features by leveraging cross-validation 
kfold = KFold(n_splits=6, random_state=42, shuffle=False)
rfecv = RFECV(estimator=sgd_reg, step=1, cv=kfold)
rfecv.fit(X, y)

# Select columns based on recursive feature extraction to be use for linear regression model
# cols = X.columns[rfecv.support_]
cols_a = ['CHAS', 'NOX', 'RM', 'DIS', 'PTRATIO', 'LSTAT']
cols_b = ['CHAS', 'NOX', 'RM', 'DIS', 'PTRATIO', 'log_LSTAT']

print("Optimal number of features: %d" % rfecv.n_features_)
print("List of optimal features: ", X.columns[rfecv.support_])
# print("Assigned score of optimal features: ", rfecv.grid_scores_)

sgd_model_a = sgd_reg.fit(Stan_X[cols_a], Stan_y)
print("\nRegression coefficients of Model A: ", sgd_model_a.coef_)
print("Intercept coefficient of Model A: ", sgd_model_a.intercept_)
# print("Rank of Model A: ", reg_model_a.rank_)
print("R^2 Model A: ", sgd_model_a.score(Stan_X[cols_a], Stan_y))
print("R^2adj Model A: ", r_squared_adj(sgd_model_a.score(Stan_X[cols_a], Stan_y), len(y), len(cols_a)))

sgd_model_b = sgd_reg.fit(Stan_X[cols_b], Stan_y)
print("\nRegression coefficients of Model B: ", sgd_model_b.coef_)
print("Intercept coefficient of Model B: ", sgd_model_b.intercept_)
# print("Rank of Model B: ", reg_model_b.rank_)
print("R^2 Model B: ", sgd_model_b.score(Stan_X[cols_b], Stan_y))
print("R^2adj Model B: ", r_squared_adj(sgd_model_b.score(Stan_X[cols_b], Stan_y), len(y), len(cols_b)))


# In[27]:

from sklearn.model_selection import cross_val_score, KFold
from sklearn.linear_model import LinearRegression
from sklearn.feature_selection import RFECV
from sklearn.preprocessing import StandardScaler

# split into input (X) and target (y) variables
X = df_boston[["ZN", "INDUS", "CHAS", "NOX", "RM", "AGE", "DIS", "RAD", "TAX", "PTRATIO", "B", "LSTAT", "log_LSTAT"]]
y = df_boston["MEDV"].values

scalerX = StandardScaler().fit(X)
Stan_X = pd.DataFrame(scalerX.transform(X), index=X.index, columns=X.columns)
scalerY = StandardScaler().fit(y)
Stan_y = scalerY.transform(y)
print(X.index[0:10])

# Create regression estimator and set hyper-parameters
lin_reg = LinearRegression(fit_intercept = True, normalize = False)

# Create the rfe object to recursively select best features by leveraging cross-validation 
kfold = KFold(n_splits=6, random_state=42, shuffle=False)
rfecv = RFECV(estimator=lin_reg, step=1, cv=kfold)
rfecv.fit(X, y)

# Select columns based on recursive feature extraction to be use for linear regression model
cols = X.columns[rfecv.support_]
cols_a = cols[[x for x in (0, 1, 2, 3, 4, 5)]]
cols_b = cols[[x for x in (0, 1, 2, 3, 4, 6)]]

print("Optimal number of features: %d" % rfecv.n_features_)
print("List of optimal features: ", X.columns[rfecv.support_])
# print("Assigned score of optimal features: ", rfecv.grid_scores_)

reg_model_a = lin_reg.fit(Stan_X[cols_a], Stan_y)
print("\nRegression coefficients of Model A: ", reg_model_a.coef_)
print("Intercept coefficient of Model A: ", reg_model_a.intercept_)
# print("Rank of Model A: ", reg_model_a.rank_)
print("R^2 Model A: ", reg_model_a.score(Stan_X[cols_a], Stan_y))
print("R^2adj Model A: ", r_squared_adj(reg_model_a.score(Stan_X[cols_a], Stan_y), len(y), len(cols_a)))

reg_model_b = lin_reg.fit(Stan_X[cols_b], Stan_y)
print("\nRegression coefficients of Model B: ", reg_model_b.coef_)
print("Intercept coefficient of Model B: ", reg_model_b.intercept_)
# print("Rank of Model B: ", reg_model_b.rank_)
print("R^2 Model B: ", reg_model_b.score(Stan_X[cols_b], Stan_y))
print("R^2adj Model B: ", r_squared_adj(reg_model_b.score(Stan_X[cols_b], Stan_y), len(y), len(cols_b)))


# In[10]:

from sklearn.model_selection import cross_val_score, KFold
from sklearn.linear_model import LinearRegression
from sklearn.feature_selection import RFECV
# from sklearn.preprocessing import StandardScaler

# split into input (X) and target (y) variables
X = df_boston[["ZN", "INDUS", "CHAS", "NOX", "RM", "AGE", "DIS", "RAD", "TAX", "PTRATIO", "B", "LSTAT", "log_LSTAT"]]
y = df_boston["MEDV"].values

# Create regression estimator and set hyper-parameters
lin_reg = LinearRegression(fit_intercept = True, normalize = True)

# Create the rfe object to recursively select best features by leveraging cross-validation 
kfold = KFold(n_splits=6, random_state=42, shuffle=False)
rfecv = RFECV(estimator=lin_reg, step=1, cv=kfold)
rfecv.fit(X, y)

# Select columns based on recursive feature extraction to be use for linear regression model
cols = X.columns[rfecv.support_]
cols_a = cols[[x for x in (0, 1, 2, 3, 4, 5)]]
cols_b = cols[[x for x in (0, 1, 2, 3, 4, 6)]]

print("Optimal number of features: %d" % rfecv.n_features_)
# print("List of optimal features: ", X.columns[rfecv.support_])
# print("Assigned score of optimal features: ", rfecv.grid_scores_)

reg_model_a = lin_reg.fit(X[cols_a], y)
print("\nRegression coefficients of Model A: ", reg_model_a.coef_)
print("Intercept coefficient of Model A: ", reg_model_a.intercept_)
# print("Rank of Model A: ", reg_model_a.rank_)
print("R^2 Model A: ", reg_model_a.score(X[cols_a], y))
print("R^2adj Model A: ", r_squared_adj(reg_model_a.score(X[cols_a], y), len(y), len(cols_a)))

reg_model_b = lin_reg.fit(X[cols_b], y)
print("\nRegression coefficients of Model B: ", reg_model_b.coef_)
print("Intercept coefficient of Model B: ", reg_model_b.intercept_)
# print("Rank of Model B: ", reg_model_b.rank_)
print("R^2 Model B: ", reg_model_b.score(X[cols_b], y))
print("R^2adj Model B: ", r_squared_adj(reg_model_b.score(X[cols_b], y), len(y), len(cols_b)))


# In[26]:

import sklearn.metrics 

y = df_boston["MEDV"]
X = sm.add_constant(df_boston[["RM", "LSTAT", "PTRATIO"]])
res = sm.OLS(y, X).fit()
print("Mean of residuals: ", np.mean(res.resid))
print("RMSE Model: ", res.mse_model)
print("RMSE Residual: ", res.mse_resid)
print("RMSE Total: ", res.mse_total)
print("ESS: ", res.ess)
print("SSR: ", res.ssr)
print("Uncentered TSS: ", res.uncentered_tss)
print("Centered TSS: ", res.centered_tss, "\n")

print("R-Squared: ", res.rsquared)
print("R-Squared-Adj: ", res.rsquared_adj, "\n")
# print(res.summary())

y_pred = res.predict()

print("RMSE: ", mean_squared_error(y, y_pred))
print("R^2: ", r2_score(y, y_pred))
print("R^2adj: ", ((1 - (r2_score(y, y_pred))**2) * (len(y) - 1)) / (len(y) - 4))


# Plot the qq-plot of the model's residuals to verify normal distribution
# sm.qqplot(res.resid, line="q")
# pylab.show()

# Plot a scatterplot of the residuals vs. the target variable to check the randomness of the residuals
# plt.plot(df_boston["MEDV"], res.resid, marker=".", linestyle="none")
# plt.xlabel("Median House Vaues")
# plt.ylabel("Residuals of Regression Model")
# plt.show()


# In[7]:

from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

def baseline_model():
    # create model
    model = Sequential()
    model.add(Dense(13, input_dim=13, init='normal', activation='relu'))
    # model.add(Dense(6, init='normal', activation='relu'))
    model.add(Dense(1, init='normal'))
    # Compile model
    model.compile(loss='mean_squared_error', optimizer='adam')
    return model

dataset = df_boston.iloc[:,0:14].values
# split into input (X) and output (Y) variables
X = dataset[:,0:13]
Y = dataset[:,13]
print(type(dataset))

# fix random seed for reproducibility
seed = 7
np.random.seed(seed)
# evaluate model with standardized dataset
estimator = KerasRegressor(build_fn=baseline_model, nb_epoch=100, batch_size=5, verbose=0)
estimator.fit(X, Y)

# kfold = KFold(n_splits=10, random_state=seed)
# results = cross_val_score(estimator, X, Y, cv=kfold)
# print("The MSE values in each kFold:", results, "\n")
# print("Results: %.2f (%.2f) MSE" % (results.mean(), results.std()))

y_pred = estimator.predict(X)
print("RMSE: ", mean_squared_error(Y, y_pred))
print("R^2: ", r2_score(Y, y_pred))
print("R^2adj: ", ((1 - (r2_score(Y, y_pred))**2) * (len(Y) - 1)) / (len(Y) - 14))

# np.random.seed(seed)
# estimators = []
# estimators.append(('standardize', StandardScaler()))
# estimators.append(('mlp', KerasRegressor(build_fn=baseline_model, nb_epoch=100, batch_size=5, verbose=0)))
# pipeline = Pipeline(estimators)
# kfold = KFold(n_splits=10, random_state=seed)
# results = cross_val_score(pipeline, X, Y, cv=kfold)
# print("Larger: %.2f (%.2f) MSE" % (results.mean(), results.std()))


# In[9]:

######## Important assumptions of the regression technique ####################
# 1. There is a linear relationship between the inputs and the target variable
# 2. The input variables are not heavily correlated with each other in order to avoid multi-collinearity
# 3. The residuals e = y(actual) - y(predicted) are normally distributed with a mean of zero
# 4. The input and target variables are normally distributed
###############################################################################

y = df_boston["MEDV"]
Xlog = sm.add_constant(df_boston[["RM", "log_LSTAT", "PTRATIO"]])
# Xlog = data[["RM", "log_LSTAT"]]
res = sm.OLS(y, Xlog).fit()

print("Mean of residuals: ", np.mean(res.resid), "\n")
# print(res.summary())

# sm.qqplot(res.resid, line="q")
# pylab.show()
plt.plot(df_boston["MEDV"], res.resid, marker=".", linestyle="none")
plt.xlabel("Median House Vaues")
plt.ylabel("Residuals of Regression Model")
plt.show()


# In[12]:

import statsmodels.formula.api as smf

def forward_selected(data, response):
    """Linear model designed by forward selection.

    Parameters:
    -----------
    data : pandas DataFrame with all possible predictors and response

    response: string, name of response column in data

    Returns:
    --------
    model: an "optimal" fitted statsmodels linear model
           with an intercept
           selected by forward selection
           evaluated by adjusted R-squared
    """
    remaining = set(data.columns)
    remaining.remove(response)
    selected = []
    current_score, best_new_score = 0.0, 0.0
    while remaining and current_score == best_new_score:
        scores_with_candidates = []
        for candidate in remaining:
            formula = "{} ~ {} + 1".format(response,
                                           ' + '.join(selected + [candidate]))
            score = smf.ols(formula, data).fit().rsquared_adj
            scores_with_candidates.append((score, candidate))
        scores_with_candidates.sort()
        best_new_score, best_candidate = scores_with_candidates.pop()
        if current_score < best_new_score:
            remaining.remove(best_candidate)
            selected.append(best_candidate)
            current_score = best_new_score
    formula = "{} ~ {} + 1".format(response,
                                   ' + '.join(selected))
    model = smf.ols(formula, data).fit()
    return model

# Test forward selection regression model
model = forward_selected(df_boston, 'MEDV')

print(model.model.formula)
print("The R^2 is: ", model.rsquared, "\nThe R^2 adjusted is: ", model.rsquared_adj)


# In[13]:

###### import pydotplus   
from IPython.display import Image 
from sklearn import tree
from sklearn.tree import DecisionTreeRegressor
# from sklearn.ensemble import AdaBoostRegressor

X = df_boston[["ZN", "INDUS", "CHAS", "NOX", "RM", "AGE", "DIS", "RAD", "TAX", "PTRATIO", "B", "log_LSTAT"]]
y = df_boston["MEDV"].values

regr_1 = DecisionTreeRegressor(max_depth=6)
regr_1.fit(X, y)
# y_1 = regr_1.predict(X)
print("Feature importance: ", regr_1.feature_importances_)
print("R^2 Model DTRegressor: ", regr_1.score(X, y))
print("R^2adj Model DTRegressor: ", r_squared_adj(regr_1.score(X, y), len(y), 6))


dot_data = tree.export_graphviz(regr_1, out_file=None, 
                                feature_names=["ZN", "INDUS", "CHAS", "NOX", "RM", "AGE", "DIS", "RAD", "TAX", "PTRATIO", "B", "log_LSTAT"],
                                class_names="MEDV", 
                                max_depth=3, 
                                filled=True, 
                                proportion=True)  
graph = pydotplus.graph_from_dot_data(dot_data)  
Image(graph.create_png())   


# In[30]:

# help center
help(pd.Series.astype)
