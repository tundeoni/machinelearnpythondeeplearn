"@author Tunde Oni"


import math
import numpy as np
import pylab as plt

def tri_num(a, b):
    """find and return all the triangular integers in [a,b]"""

    #find smallest n such that n*(n+1)/2 >= a, by solving n**2 + n - 2*a >=0 
    delta = 1 + 4*2*a
    if delta >= 0:
        nmin = math.ceil((-1 + delta**.5)/2)
    else:
        nmin = 0

    #find largest n such that n*(n+1)/2 <= b, by solving n**2 + n - 2*b <=0 
    delta = 1 + 4*2*b
    if delta >= 0:
        nmax = math.floor((-1 + delta**.5)/2)
    else:
        nmax = 0    

    if a<=0 and nmin>0:  nmin=0   #because 0 is always a tri num

    tri_int=[]    
    for n in range(nmin, nmax+1):
        tri_int.append(n*(n+1)//2)

    #test that the integers are within [a, b]
    if tri_int[0] < max(a, 0) or tri_int[-1] > max(b, 0): 
        raise Exception('a={}, tri[0]={}, tri[-1]={}, b={}'.format(a, tri_int[0], tri_int[-1], b))
    if (nmin-1)*nmin/2>=max(a,1) or (nmax+1)*(nmax+2)/2<=max(b, -1): 
        print('a={}, leftn={}, rightn={}, b={}'.format(a, nmin*(nmin-1)/2, (nmax+1)*(nmax+2)/2, b))
        raise Exception('your nmin or nmax are incorrect')

    if len(tri_int) != nmax-nmin+1:
        print(' len(tri_int)={},  theoretic formula ={}'.format(len(tri_int), nmax-nmin+1))   
        raise Exception('theoretical formula does not agree with computation')

    print(' # of tri_int inside [{}, {}] ={}'. format(a, b, len(tri_int)))   
    return tri_int



if __name__=='__main__':

    mrange = range(-10**6, 10**6+1, 10**3);  mlen = (len(mrange)-1)//2
    fm = np.zeros(len(mrange))  #fm=0 at m=0, this 0 fm value is not updated

    for i, m in enumerate(mrange[mlen+1:],1):   #since fm() is even, only need to compute half of fm()
        fm[mlen+i] = len(tri_num(-abs(m), abs(m)))
        fm[mlen-i] = fm[mlen+i]

    print(fm)
    plt.plot(mrange, fm, '-+')
    #plt.plot([float(x) for x in mrange], fm, '-+') #change x ticks to float (no use)
    plt.show()


    #can also use list instead of np array
    fm2 = [0]
    mrange = range(-10**6, 10**6+1, 10**3)
    for m in range(10**3, 10**6+1, 10**3): 
        tmp = len(tri_num(0, abs(m)))              #note:  #[-|m|, |m|] = #[0,|m|]
        fm2.append(tmp)
        fm2.insert(0, tmp)

    #print(fm2) 
    print('fm==fm2 is {}'.format(list(fm)==fm2))   #for verification
    plt.plot(mrange, fm2, '-+')
    plt.show()    