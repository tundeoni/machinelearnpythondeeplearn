"@author: tunde"

#
#Compute 1/9801 upto 1000 decimal digits using the decimal module
#

import decimal

#
#set and show the effect of different precision
#
for precision in range(10, 220, 10):
    decimal.getcontext().prec = precision
    print('precision={:3d}, 1/9801={}'.format(precision, 1/decimal.Decimal(9801)))

print('\nObserve from above: \nthe nice pattern breaks down when the 97 is not followed by 98 but instead by 99, as seen when precision>=200\n')

#from problem 9
precision = 1000
decimal.getcontext().prec = precision
print('precision={:3d}, 1/9801={}'.format(precision, decimal.Decimal(1)/9801))