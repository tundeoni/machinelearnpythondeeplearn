" @author: tunde"

import time

#append to list one by one 
def int_div_reverse(a, b, step=1):
    int_list = []
    for n in range(a, b+1, step):
        if  n % reverse(n) == 0: 
            int_list.append(n)
            #print(int_list)

    return int_list


#can also use filter 
def int_div_reverse_filter(a, b, step=1):
    return list(filter(lambda n: n% reverse(n)==0, range(a, b+1, step)))


def reverse(n):
    """ returns the reverse integer of n """
    return int(str(n)[-1::-1])


def reverse2(n):  #this guy is slower
    """ returns the reverse integer of n """ 
    n, mod = divmod(n, 10);  a=str(mod)
    while n >= 1:
        n, mod = divmod(n, 10);  a +=str(mod)
    
    return int(a)


if __name__=='__main__':

    import time
    n = 10**7     #n=10**9 may take too long to finish
    t0 = time.clock()
    intlist = int_div_reverse_filter(1, n)
    print('len(intlist)={}, filter, time used={} seconds'.format(len(intlist), time.clock()-t0))

    t0 = time.clock()
    intlist2 = int_div_reverse(1, n)
    print('len(intlist2)={}, time used={} seconds'.format(len(intlist2), time.clock()-t0))

    k = 50
    if len(intlist) <= k:
        print(intlist)
    else:
        #print the last k numbers in intlist 
        print('the last {} numbers in list are {}'.format(k, intlist[len(intlist)-k:]))  

    print('intlist==intlist2 is',  intlist==intlist2)