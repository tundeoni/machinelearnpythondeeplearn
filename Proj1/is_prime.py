"""
Created on Mon Mar  6 19:38:51 2017

@author: tunde
"""

import math
def is_prime(prm):
    if prm % 2 == 0 and prm > 2: 
        return False
    return all(prm % i for i in range(3, int(math.sqrt(prm)) + 1, 2))


""" a """
is_prime(20170123456789)

""" b """
is_prime(2016**4+2017**4+2018**4)

""" c """
is_prime(math.ceil(2017*math.pi))

""" d"""
is_prime(math.ceil(2017*math.e))

f = lambda x: (x*6)+7

is_prime(f(2017))
is_prime(f(f(2017)))
is_prime(f(f(f(2017))))